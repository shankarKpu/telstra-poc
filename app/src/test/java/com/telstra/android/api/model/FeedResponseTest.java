package com.telstra.android.api.model;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

public class FeedResponseTest {

    private final String title = "Testing Title";

    private final List<Row> rowsList = new ArrayList<>();

    @Mock
    FeedResponse feedResponse;

    @Mock
    List<Row> rows;

    @Before
    public void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
        Mockito.when(feedResponse.getTitle()).thenReturn(title);
        Mockito.when(feedResponse.getRows()).thenReturn(rowsList);

    }


    @Test
   public void testFeedTitle(){
        Mockito.when(feedResponse.getTitle()).thenReturn(title);
        Assert.assertEquals("Testing Title",feedResponse.getTitle());

   }

   @Test
   public void testFeedTitleIncorrect(){
        Mockito.when(feedResponse.getTitle()).thenReturn(title);
        Assert.assertNotEquals("Title",feedResponse.getTitle());

   }

   @Test
   public void testRow(){
        Mockito.when(feedResponse.getRows()).thenReturn(rowsList);
        Assert.assertEquals(new ArrayList<>(),feedResponse.getRows());
   }

   @Test
    public void testRowIncorrect(){

        Mockito.when(feedResponse.getRows()).thenReturn(rowsList);
        Assert.assertNotEquals("",feedResponse.getRows());
   }

   @After
   public void tearDown() throws Exception{
        feedResponse=null;
        rows=null;
   }

}
