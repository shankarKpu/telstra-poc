package com.telstra.android.api.model;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class RowTest {

    private final String title = "Testing Title";
    private final String description = "Testing description";
    private final String imageHref = "www.google.com/image";


    @Mock
    Row row;


    @Before
    public void setUp() throws Exception{

        MockitoAnnotations.initMocks(this);
        Mockito.when(row.getTitle()).thenReturn(title);
        Mockito.when(row.getDescription()).thenReturn(description);
        Mockito.when(row.getImageHref()).thenReturn(imageHref);


    }


    @Test
    public void testTitle(){
        Mockito.when(row.getTitle()).thenReturn(title);
        Assert.assertEquals("Testing Title",row.getTitle());

    }

    @Test
    public void testDescription(){
        Mockito.when(row.getDescription()).thenReturn(description);
        Assert.assertEquals("Testing description",row.getDescription());

    }
    @Test
    public void testImageHref(){
        Mockito.when(row.getImageHref()).thenReturn(imageHref);
        Assert.assertEquals("www.google.com/image",row.getImageHref());

    }


    @Test
    public void testTitleIncorrect(){
        Mockito.when(row.getTitle()).thenReturn(title);
        Assert.assertNotEquals("Title",row.getTitle());

    }

    @Test
    public void testDescriptionIncorrect(){
        Mockito.when(row.getDescription()).thenReturn(description);
        Assert.assertNotEquals("description",row.getDescription());

    }
    @Test
    public void testImageHrefIncorrect(){
        Mockito.when(row.getImageHref()).thenReturn(imageHref);
        Assert.assertNotEquals("www.google.com",row.getImageHref());

    }
    @After
    public void tearDown() throws Exception{
        row=null;
    }


}
