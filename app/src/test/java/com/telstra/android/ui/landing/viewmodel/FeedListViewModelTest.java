package com.telstra.android.ui.landing.viewmodel;


import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.Observer;
import android.content.Context;

import com.telstra.android.api.ApiEndPoint;
import com.telstra.android.api.AppApiClient;
import com.telstra.android.api.RxSingleSchedulers;
import com.telstra.android.api.model.FeedResponse;
import com.telstra.android.api.ApiConstants;
import com.telstra.android.utils.rx.SchedulerProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class FeedListViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    FeedListViewModel feedViewModel;
    ApiConstants feedApi;
    @Mock
    ApiEndPoint apiEndPoint;
    @Mock
    AppApiClient apiClient;
    private FeedListViewModel viewModel;
    @Mock
    Observer<FeedListViewState> observer;
    @Mock
    LifecycleOwner lifecycleOwner;
    Lifecycle lifecycle;



    @Before
    public void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
        lifecycle = new LifecycleRegistry(lifecycleOwner);
        viewModel = new FeedListViewModel(apiClient, RxSingleSchedulers.TEST_SCHEDULER);
        viewModel.getNewsListState().observeForever(observer);

    }

    @Test
    public void testNull() {
        when(apiClient.fetchFeeds()).thenReturn(null);
        assertNotNull(viewModel.getNewsListState());
        assertTrue(viewModel.getNewsListState().hasObservers());
    }

    @Test
    public void testApiFetchDataSuccess() {
        // Mock API response
        when(apiClient.fetchFeeds()).thenReturn(Single.just(new FeedResponse()));
        viewModel.fetchFeeds();
        verify(observer).onChanged(FeedListViewState.LOADING_STATE);
        verify(observer).onChanged(FeedListViewState.SUCCESS_STATE);
    }

    @Test
    public void testApiFetchDataError() {
        when(apiClient.fetchFeeds()).thenReturn(Single.error(new Throwable("Api error")));
        viewModel.fetchFeeds();
        verify(observer).onChanged(FeedListViewState.LOADING_STATE);
        verify(observer).onChanged(FeedListViewState.ERROR_STATE);
    }
    @After
    public void tearDown() throws Exception {
        apiClient = null;
        viewModel = null;
    }





}
