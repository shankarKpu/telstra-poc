package com.telstra.android.api;


import com.telstra.android.api.model.FeedResponse;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 *  Created by Shankar K on 23-08-2019.
 */

public class AppApiClient {

    private final ApiEndPoint api;

    @Inject
    public AppApiClient(ApiEndPoint api) {
        this.api = api;
    }

    public Single<FeedResponse> fetchFeeds() {
        return api.getFeeds();
    }


}
