package com.telstra.android.api.model;

import android.databinding.BindingAdapter;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.telstra.android.R;

import lombok.Data;

/**
 * Created by Shankar K on 23-08-2019.
 */
@Data
public class Row implements Parcelable {
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("imageHref")
    @Expose
    public String imageHref;

    @BindingAdapter({ "imageHref" })
    public static void loadImage(ImageView imageView, String imageURL) {

        Glide.with(imageView.getContext())
                .setDefaultRequestOptions(new RequestOptions().override(170,170)
                        .circleCrop())
                .load(imageURL)
                .placeholder(R.drawable.loading_img)
                .error(R.drawable.no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)

                .into(imageView);
    }

    public Row(){

    }
    public Row(Parcel in) {
        title = in.readString();
        description = in.readString();
        imageHref = in.readString();
    }

    public static final Creator<Row> CREATOR = new Creator<Row>() {
        @Override
        public Row createFromParcel(Parcel in) {
            return new Row(in);
        }

        @Override
        public Row[] newArray(int size) {
            return new Row[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(imageHref);
    }
}
