package com.telstra.android.api;


import com.telstra.android.api.model.FeedResponse;

import io.reactivex.Single;
import retrofit2.http.GET;

import static com.telstra.android.api.ApiConstants.FEEDS;

/**
 *  Created by Shankar K on 23-08-2019.
 */

public interface ApiEndPoint {

    @GET( FEEDS )
    Single<FeedResponse> getFeeds();




}
