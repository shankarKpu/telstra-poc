package com.telstra.android.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

/**
 * Created by Shankar K on 23-08-2019.
 */
@Data
public class FeedResponse implements Parcelable {
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("rows")
    @Expose
    public List<Row> rows = null;

    public FeedResponse(){

    }

    protected FeedResponse(Parcel in) {
        title = in.readString();
        rows = in.createTypedArrayList(Row.CREATOR);
    }

    public static final Creator<FeedResponse> CREATOR = new Creator<FeedResponse>() {
        @Override
        public FeedResponse createFromParcel(Parcel in) {
            return new FeedResponse(in);
        }

        @Override
        public FeedResponse[] newArray(int size) {
            return new FeedResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeTypedList(rows);
    }
}
