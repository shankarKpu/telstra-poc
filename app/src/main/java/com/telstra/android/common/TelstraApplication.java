package com.telstra.android.common;

import android.app.Activity;
import android.support.multidex.MultiDexApplication;


import com.telstra.android.di.component.DaggerApplicationComponent;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * Created by Shankar K on 23-08-2019.

 * Application class for the dagger injected
 */
public class TelstraApplication extends MultiDexApplication implements HasActivityInjector {
    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;


    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }

    @Override
    public void onCreate() {
        super.onCreate();
       DaggerApplicationComponent
               .builder()
                .application(this)
                .build()
                .inject(this);
    }
}
