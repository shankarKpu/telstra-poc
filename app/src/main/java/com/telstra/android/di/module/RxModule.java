package com.telstra.android.di.module;

import com.telstra.android.api.RxSingleSchedulers;
import com.telstra.android.di.scope.AppScope;

import dagger.Module;
import dagger.Provides;
/**
 * Created by Shankar K on 23-08-2019.
 */
@Module
public class RxModule {
    @AppScope
    @Provides
    public RxSingleSchedulers providesScheduler() {
        return RxSingleSchedulers.DEFAULT;
    }
}
