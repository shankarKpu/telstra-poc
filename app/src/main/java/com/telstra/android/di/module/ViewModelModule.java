package com.telstra.android.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.telstra.android.common.ViewModelProviderFactory;
import com.telstra.android.di.scope.ViewModelKey;
import com.telstra.android.ui.landing.viewmodel.FeedListViewModel;
import com.telstra.android.ui.splash.viewmodel.SplashViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by Shankar K on 23-08-2019.
 */
@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(FeedListViewModel.class)
    abstract ViewModel bindFeedListViewModel(FeedListViewModel feedListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel.class)
    abstract ViewModel bindSplashViewModel(SplashViewModel splashViewModel);


    @Binds
    abstract ViewModelProvider.Factory
    bindViewModelProviderFactory(ViewModelProviderFactory factory);
}
