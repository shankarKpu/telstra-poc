package com.telstra.android.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.telstra.android.BuildConfig;
import com.telstra.android.api.ApiEndPoint;
import com.telstra.android.di.scope.AppScope;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
/**
 * Created by Shankar K on 23-08-2019.
 */
@Module
public class ApiModule {


    @AppScope
    @Provides
    OkHttpClient provideOkHttpClient(){

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor( );
        loggingInterceptor.setLevel( HttpLoggingInterceptor.Level.BODY );
        OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder( )
                        .connectTimeout(1, TimeUnit.MINUTES)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(15, TimeUnit.SECONDS)
                        .addInterceptor( loggingInterceptor )
                        .addInterceptor( chain -> {
                            Request original = chain.request( );
                            HttpUrl originalHttpUrl = original.url( );

                            HttpUrl url = originalHttpUrl.newBuilder( )
                                    .build( );
                            // Request customization: add request headers
                            Request.Builder requestBuilder = original.newBuilder( )
                                    .url( url );

                            Request request = requestBuilder.build( );
                            return chain.proceed( request );
                        } );
        return httpClient.build();
    }

    @AppScope
    @Provides
    Retrofit provideRetrofit(Gson gson ,OkHttpClient okHttpClient) {

        return new Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @AppScope
    @Provides
    Gson provideGson() {
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }



    @AppScope
    @Provides
    ApiEndPoint provideNewsApi(Retrofit retrofit) {
        return retrofit.create(ApiEndPoint.class);
    }
}
