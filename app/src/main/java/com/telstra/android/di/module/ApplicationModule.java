package com.telstra.android.di.module;

import android.app.Application;
import android.content.Context;

import dagger.Binds;
import dagger.Module;

/**
 * Created by Shankar K on 23-08-2019.
 */
@Module(includes = ViewModelModule.class)
abstract public class ApplicationModule {

    @Binds
    abstract Context provideContext(Application application);

}
