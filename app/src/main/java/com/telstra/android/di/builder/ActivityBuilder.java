package com.telstra.android.di.builder;



import com.telstra.android.ui.landing.FeedListActivityModule;
import com.telstra.android.ui.landing.view.FeedListActivity;
import com.telstra.android.ui.splash.view.SplashActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 *  Created by Shankar K on 23-08-2019.
 */

@Module
public abstract class ActivityBuilder {

   @ContributesAndroidInjector()
   abstract SplashActivity bindSplashActivity();

   @ContributesAndroidInjector(modules = {FeedListActivityModule.class})
   abstract FeedListActivity bindFeedListActivity();
}

