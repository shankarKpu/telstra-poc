package com.telstra.android.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;
import javax.inject.Scope;

/**
 *  Created by Shankar K on 23-08-2019.
 */
@Scope
@Retention( RetentionPolicy.RUNTIME)
public @interface AppScope {
}
