package com.telstra.android.di.component;

import android.app.Application;


import com.telstra.android.common.TelstraApplication;
import com.telstra.android.di.builder.ActivityBuilder;
import com.telstra.android.di.module.ApiModule;
import com.telstra.android.di.module.ApplicationModule;
import com.telstra.android.di.module.RxModule;
import com.telstra.android.di.scope.AppScope;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 *  Created by Shankar K on 23-08-2019.
 */
@AppScope
@Component(modules = {ApplicationModule.class, AndroidSupportInjectionModule.class,
        ActivityBuilder.class, ApiModule.class, RxModule.class})
public interface ApplicationComponent extends AndroidInjector<TelstraApplication> {
    void inject(TelstraApplication app);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        ApplicationComponent build();
    }
}
