package com.telstra.android.utils;

import android.content.res.Resources;


/**
 *  Created by Shankar K on 23-08-2019.
 */
public final class AppConstants {



    public static final String NO_TITLE = "No Title";
    public static final String NO_DESCRIPTION = "No Description Available";
    public static final String NETWORK_ERROR = "Oops! Some Thing went wrong";
    public static final String NETWORK_STATUS="Oops! Please check your internet connection";


}
