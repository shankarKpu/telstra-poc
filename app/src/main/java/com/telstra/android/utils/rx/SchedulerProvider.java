package com.telstra.android.utils.rx;

import io.reactivex.Scheduler;

/**
 *  Created by Shankar K on 23-08-2019.
 */
public interface SchedulerProvider {
    Scheduler computation();

    Scheduler io();

    Scheduler ui();
}
