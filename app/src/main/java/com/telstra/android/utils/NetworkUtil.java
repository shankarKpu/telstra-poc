package com.telstra.android.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 *  Created by Shankar K on 23-08-2019.
 */

public class NetworkUtil {
    /**
     * Network Availability in the device, true is available and false indicates no network
     * connection
     */
    public static boolean isConnected(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();

        if (activeNetwork == null || !activeNetwork.isConnected()) {
            return false;
        } else {
            return activeNetwork != null
                    && activeNetwork.isConnectedOrConnecting();
        }
    }
}
