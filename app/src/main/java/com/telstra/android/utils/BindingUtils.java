package com.telstra.android.utils;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;

import com.bumptech.glide.request.RequestListener;

import com.telstra.android.api.model.Row;
import com.telstra.android.ui.landing.view.FeedsItemAdapter;

import java.util.List;

/**
 *  Created by Shankar K on 23-08-2019.
 * Binding Class
 */

public final class BindingUtils {

    public RequestListener<Bitmap> requestListener;

    private BindingUtils() {
        // This class is not publicly instantiable
    }

    /**
     * Row Item Binding for adapter
     * @param recyclerView
     * @param rowItem
     */
    @BindingAdapter({"adapter"})
    public static void addRowItem(RecyclerView recyclerView, List<Row> rowItem) {
        FeedsItemAdapter adapter = (FeedsItemAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.clearItems();
            adapter.addRowList(rowItem);
        }
    }





}
