package com.telstra.android.ui.landing.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;


import com.telstra.android.api.model.Row;
import com.telstra.android.databinding.EmptyItemViewModelBinding;
import com.telstra.android.databinding.FeedItemsBinding;
import com.telstra.android.di.scope.AppScope;
import com.telstra.android.ui.base.BaseViewHolder;
import com.telstra.android.ui.landing.viewmodel.EmptyItemViewModel;
import com.telstra.android.ui.landing.viewmodel.ItemViewModel;

import java.util.ArrayList;
import java.util.List;
/**
 *  Created by Shankar K on 23-08-2019.
 */

/**
 * Feed Adapter which will bind the row item into recyclerview
 */

public class FeedsItemAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private final List<Row> mResponseList;
    private final Context mContext;


    private FeedAdapterListener mListener;

    public FeedsItemAdapter(@AppScope Context context) {
        this.mContext = context;
        this.mResponseList = new ArrayList<>();
    }


    @Override
    public int getItemCount() {
        if (!mResponseList.isEmpty()) {
            return mResponseList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (!mResponseList.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:

                FeedItemsBinding feedItemsBinding = FeedItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
                parent.setTag(feedItemsBinding);
                return new FeedListViewHolder(feedItemsBinding);

            case VIEW_TYPE_EMPTY:
                EmptyItemViewModelBinding emptyViewBinding = EmptyItemViewModelBinding
                        .inflate(LayoutInflater.from(parent.getContext()), parent, false);
                return new EmptyViewHolder(emptyViewBinding);
            default:
                return null;
        }
    }

    public void addRowList(List<Row> repoList) {
        if (!this.mResponseList.isEmpty()) {
            this.mResponseList.clear();
        }
        this.mResponseList.addAll(repoList);
        notifyDataSetChanged();
    }



    public void clearItems() {
        mResponseList.clear();
        notifyDataSetChanged();
    }

    public void setListener(FeedAdapterListener listener) {
        this.mListener = listener;
    }

    public interface FeedAdapterListener {

        void onRetryClick();
    }

    /**
     * If row item is empty this view holder will render in the UI
     */
    public class EmptyViewHolder extends BaseViewHolder implements EmptyItemViewModel.EmptyItemViewModelListener {

        private final EmptyItemViewModelBinding mBinding;

        public EmptyViewHolder(EmptyItemViewModelBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            EmptyItemViewModel emptyItemViewModel = new EmptyItemViewModel(this);
            mBinding.setEmptyFeedView(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }


    /**
     * Each item while render in the view holder
     */
    public class FeedListViewHolder extends BaseViewHolder implements ItemViewModel.FeedItemListener {


        private Row rowModel;
        private FeedItemsBinding itemViewModelBinding;
        private ItemViewModel itemViewModel;


        public FeedListViewHolder(FeedItemsBinding binding) {
            super(binding.getRoot());
            this.itemViewModelBinding = binding;
        }

        @Override
        public void onBind(int position) {
            rowModel = mResponseList.get(position);
            itemViewModel = new ItemViewModel(rowModel, this);
            itemViewModelBinding.setFeedItemViewModel(itemViewModel);
            itemViewModelBinding.executePendingBindings();
        }

        @Override
        public void onItemClick(Row row) {

        }


    }
}
