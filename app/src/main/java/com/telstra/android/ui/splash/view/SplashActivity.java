package com.telstra.android.ui.splash.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;


import com.telstra.android.BR;
import com.telstra.android.R;
import com.telstra.android.common.ViewModelProviderFactory;
import com.telstra.android.databinding.SplashActivityBinding;
import com.telstra.android.ui.base.BaseActivity;
import com.telstra.android.ui.landing.view.FeedListActivity;
import com.telstra.android.ui.splash.viewmodel.SplashViewModel;

import javax.inject.Inject;

/**
 *  Created by Shankar K on 23-08-2019.
 */
public class SplashActivity extends BaseActivity<SplashActivityBinding>
         {

    private static final long SPLASH_TIME_OUT = 5000;
    SplashViewModel mSplashViewModel;
    @Inject
    ViewModelProviderFactory mViewModelFactory;

    SplashActivityBinding mSplashScreenBinding;

    @Override
    public int getBindingVariable() {
        return BR.splashview;
    }

    @Override
    public int getLayoutId() {
        return R.layout.splash_screen;
    }


    @Override
    public void intiBinding() {
        mSplashScreenBinding = getViewDataBinding();


    }


    public void openNextActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(FeedListActivity.newIntent(SplashActivity.this));
                finish();
            }
        }, SPLASH_TIME_OUT);


    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        intiBinding();
        mSplashViewModel = ViewModelProviders.of(this,
                mViewModelFactory).get(SplashViewModel.class);
        openNextActivity();

    }


}
