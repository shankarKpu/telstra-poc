package com.telstra.android.ui.landing.viewmodel;

/**
 *  Created by Shankar K on 23-08-2019.
 */

public class EmptyItemViewModel {
    private final EmptyItemViewModelListener mListener;

    public EmptyItemViewModel(EmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface EmptyItemViewModelListener {

        void onRetryClick();
    }
}
