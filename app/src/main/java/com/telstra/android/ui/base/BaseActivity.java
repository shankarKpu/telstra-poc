package com.telstra.android.ui.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;


import dagger.android.AndroidInjection;
import dagger.android.support.DaggerAppCompatActivity;

/**
 *  Created by Shankar K on 23-08-2019.
 * @param <T>
 * @param <V>
 */

public abstract class BaseActivity<T extends ViewDataBinding> extends DaggerAppCompatActivity {


    private T mViewDataBinding;

    public boolean isConnected;
    private ProgressDialog mProgressDialog;

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    public abstract int getBindingVariable();

    /**
     * @return layout resource Id
     */
    public abstract
    @LayoutRes
    int getLayoutId();



    /**
     * @return view data binding
     */
    public T getViewDataBinding() {
        return mViewDataBinding;
    }

    /**
     * Override for initial binding variable
     */

    public abstract void intiBinding();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        performDependencyInjection();
        super.onCreate(savedInstanceState);
        performDataBinding();
    }

    public void performDependencyInjection() {
        AndroidInjection.inject(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    protected void performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        mViewDataBinding.executePendingBindings();
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }







}
