package com.telstra.android.ui.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 *  Created by Shankar K on 23-08-2019.
 */
public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
    public BaseViewHolder(View itemView) {
        super(itemView);
    }
    public abstract void onBind(int position);
}
