package com.telstra.android.ui.landing.viewmodel;

import android.databinding.ObservableField;
import android.support.annotation.Nullable;

import com.telstra.android.api.model.Row;
import com.telstra.android.utils.AppConstants;

/**
 *  Created by Shankar K on 23-08-2019.
 */
public class ItemViewModel {
    @Nullable
    public ObservableField<String> mTitle;

    public ObservableField<String> mImageUrl;

    @Nullable
    public ObservableField<String> mDescription;


    public final FeedItemListener mListener;

    private Row mData;

    public ItemViewModel(Row data, FeedItemListener listener) {
        this.mData = data;
        this.mListener = listener;

        if (mData.getTitle() != null) {
            mTitle = new ObservableField<String>(mData.getTitle());

        }else {
            mTitle = new ObservableField<String>(AppConstants.NO_TITLE);
        }
        if (mData.getDescription() != null) {
            mDescription = new ObservableField<String>(mData.getDescription());
        }else {
            mDescription = new ObservableField<String>(AppConstants.NO_DESCRIPTION);
        }

        mImageUrl = new ObservableField<String>(mData.getImageHref());


    }

    public void onItemClick() {
        mListener.onItemClick(mData);
    }

    public interface FeedItemListener {

        void onItemClick(Row item);
    }

}
