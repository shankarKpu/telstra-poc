package com.telstra.android.ui.landing.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableList;


import com.telstra.android.api.AppApiClient;
import com.telstra.android.api.RxSingleSchedulers;
import com.telstra.android.api.model.FeedResponse;
import com.telstra.android.api.model.Row;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Shankar K on 23-08-2019.
 */
public class FeedListViewModel extends ViewModel {
    private CompositeDisposable disposable;
    private final AppApiClient appApiClient;
    private final RxSingleSchedulers rxSingleSchedulers;
    private final MutableLiveData<FeedListViewState> newsListState = new MutableLiveData<>();

    public MutableLiveData<FeedListViewState> getNewsListState() {
        return newsListState;
    }

    public MutableLiveData<String> mToolBarTitle = new MutableLiveData<>();

    public final ObservableList<Row> mFeedList = new ObservableArrayList<>();

    public MutableLiveData<String> mShowMessage = new MutableLiveData<String>();

    private final MutableLiveData<List<Row>> mFeedsData;

    private final MutableLiveData<Boolean> mSwipeLayout = new MutableLiveData<>();


    @Inject
    public FeedListViewModel(AppApiClient apiClient, RxSingleSchedulers rxSingleSchedulers) {
        this.appApiClient=apiClient;
        this.rxSingleSchedulers=rxSingleSchedulers;
        disposable = new CompositeDisposable();
        mFeedsData = new MutableLiveData<>();
    }


    public void fetchFeeds(){
        disposable.add(appApiClient.fetchFeeds()
                .doOnEvent((feedList, throwable) -> onLoading())
                .compose(rxSingleSchedulers.applySchedulers())
                .subscribe(this::onSuccess,
                        this::onError));
    }

    public MutableLiveData<String> showMessage() {
        return mShowMessage;
    }

    public MutableLiveData<String> getTitle() {
        return mToolBarTitle;
    }

    public MutableLiveData<Boolean> getSwipeLayoutStatus(){

        return mSwipeLayout;
    }

    public void onRefresh()
    {
        fetchFeeds();
    }

    private void onSuccess(FeedResponse feedResponse) {
        if (feedResponse != null && feedResponse.getRows() != null) {
            mToolBarTitle.setValue(feedResponse.getTitle());
            mFeedsData.setValue(feedResponse.getRows());
            mSwipeLayout.setValue(false);
        }
        FeedListViewState.SUCCESS_STATE.setData(feedResponse);
        newsListState.postValue(FeedListViewState.SUCCESS_STATE);
    }

    private void onError(Throwable error) {
        mSwipeLayout.setValue(false);
        FeedListViewState.ERROR_STATE.setError(error);
        newsListState.postValue(FeedListViewState.ERROR_STATE);
    }

    private void onLoading() {
        newsListState.postValue(FeedListViewState.LOADING_STATE);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }

}
