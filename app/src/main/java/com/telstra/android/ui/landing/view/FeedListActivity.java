package com.telstra.android.ui.landing.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;


import com.telstra.android.BR;
import com.telstra.android.R;
import com.telstra.android.api.model.FeedResponse;
import com.telstra.android.common.ViewModelProviderFactory;
import com.telstra.android.databinding.ActivityFeedListBinding;
import com.telstra.android.ui.base.BaseActivity;
import com.telstra.android.ui.landing.viewmodel.FeedListViewModel;

import javax.inject.Inject;

/**
 * Created by Shankar K on 23-08-2019.
 */

public class FeedListActivity extends BaseActivity<ActivityFeedListBinding>
        implements
        FeedsItemAdapter.FeedAdapterListener {

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    FeedsItemAdapter mFeedsItemAdapter;

    @Inject
    ViewModelProviderFactory mViewModelFactory;

    private FeedListViewModel mFeedListViewModel;
    private ActivityFeedListBinding mActivityFeedBinding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityFeedBinding = getViewDataBinding();
        mFeedListViewModel = ViewModelProviders.of(this,
                mViewModelFactory).get(FeedListViewModel.class);
        intiBinding();

    }


    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, FeedListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }


    @Override
    public int getBindingVariable() {
        return BR.feedsViewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.feed_list;
    }


    @Override
    public void intiBinding() {
        setUpAdapter();
        observeDataChange();
        setSupportActionBar(mActivityFeedBinding.toolbar);
        mActivityFeedBinding.progress.setVisibility(View.VISIBLE);
        mFeedListViewModel.fetchFeeds();
    }

    private void setUpAdapter() {
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mActivityFeedBinding.feedsRecyclerView.setLayoutManager(mLayoutManager);
        mActivityFeedBinding.feedsRecyclerView.setAdapter(mFeedsItemAdapter);
        mFeedsItemAdapter.setListener(this);
        mActivityFeedBinding.swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mFeedListViewModel.fetchFeeds();
            }
        });

    }

    private void observeDataChange() {
        mFeedListViewModel.getNewsListState().observe(this, newsListViewState -> {
            switch (newsListViewState.getCurrentState()) {
                case 0:
                    mActivityFeedBinding.progress.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    mActivityFeedBinding.progress.setVisibility(View.GONE);
                    loadFeedData(newsListViewState.getData());
                    break;
                case -1: // show error
                    mActivityFeedBinding.progress.setVisibility(View.GONE);
                    showMessage(newsListViewState.getError().getMessage());

                    break;
            }
        });
    }

    private void loadFeedData(FeedResponse data) {
        mFeedsItemAdapter.addRowList(data.getRows());
        mFeedListViewModel.getTitle().observe(this, this::updateToolBar);
        mFeedListViewModel.showMessage().observe(this, this::showMessage);
        mFeedListViewModel.getSwipeLayoutStatus().observe(this, this::updateSwipeLayout);

    }


    private void updateSwipeLayout(Boolean status) {

        mActivityFeedBinding.swipeToRefresh.setRefreshing(false);
    }

    private void updateToolBar(String title) {
        mActivityFeedBinding.toolbar.setTitle(title);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    protected void onResume() {
        super.onResume();

    }


    public void showMessage(String mesg) {
        Snackbar snackbar = Snackbar
                .make(mActivityFeedBinding.clRootView, mesg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }


    @Override
    public void onRetryClick() {
        mFeedListViewModel.fetchFeeds();
    }


}
