package com.telstra.android.ui.landing;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;


import com.telstra.android.di.scope.AppScope;
import com.telstra.android.ui.landing.view.FeedsItemAdapter;

import dagger.Module;
import dagger.Provides;

/**
 *  Created by Shankar K on 23-08-2019.
 */
@Module
public class FeedListActivityModule {
    @Provides
    LinearLayoutManager provideLinearLayoutManager(@AppScope Context context) {
        return new LinearLayoutManager(context);
    }

    @Provides
    FeedsItemAdapter provideOpenSourceAdapter(@AppScope Context context) {
        return new FeedsItemAdapter(context);
    }


}
