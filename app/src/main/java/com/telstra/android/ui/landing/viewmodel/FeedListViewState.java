package com.telstra.android.ui.landing.viewmodel;

import com.telstra.android.api.model.FeedResponse;
import com.telstra.android.api.model.Row;
import com.telstra.android.ui.base.BaseViewState;
/**
 * Created by Shankar K on 23-08-2019.
 */
public class FeedListViewState extends BaseViewState<FeedResponse> {
    private FeedListViewState(FeedResponse data, int currentState, Throwable error) {
        this.data = data;
        this.error = error;
        this.currentState = currentState;
    }

    public static FeedListViewState ERROR_STATE = new FeedListViewState(null, State.FAILED.value, new Throwable());
    public static FeedListViewState LOADING_STATE = new FeedListViewState(null, State.LOADING.value, null);
    public static FeedListViewState SUCCESS_STATE = new FeedListViewState(new FeedResponse(), State.SUCCESS.value, null);

}
