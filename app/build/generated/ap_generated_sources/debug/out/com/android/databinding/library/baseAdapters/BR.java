package com.android.databinding.library.baseAdapters;

public class BR {
  public static final int _all = 0;

  public static final int feedsViewModel = 1;

  public static final int feedItemViewModel = 2;

  public static final int splashview = 3;

  public static final int emptyFeedView = 4;
}
