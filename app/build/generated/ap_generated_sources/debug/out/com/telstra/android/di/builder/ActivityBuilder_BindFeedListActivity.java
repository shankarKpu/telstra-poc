package com.telstra.android.di.builder;

import android.app.Activity;
import com.telstra.android.ui.landing.FeedListActivityModule;
import com.telstra.android.ui.landing.view.FeedListActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

@Module(subcomponents = ActivityBuilder_BindFeedListActivity.FeedListActivitySubcomponent.class)
public abstract class ActivityBuilder_BindFeedListActivity {
  private ActivityBuilder_BindFeedListActivity() {}

  @Binds
  @IntoMap
  @ActivityKey(FeedListActivity.class)
  abstract AndroidInjector.Factory<? extends Activity> bindAndroidInjectorFactory(
      FeedListActivitySubcomponent.Builder builder);

  @Subcomponent(modules = FeedListActivityModule.class)
  public interface FeedListActivitySubcomponent extends AndroidInjector<FeedListActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<FeedListActivity> {}
  }
}
