// Generated by Dagger (https://google.github.io/dagger).
package com.telstra.android.di.module;

import com.telstra.android.api.RxSingleSchedulers;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

public final class RxModule_ProvidesSchedulerFactory implements Factory<RxSingleSchedulers> {
  private final RxModule module;

  public RxModule_ProvidesSchedulerFactory(RxModule module) {
    this.module = module;
  }

  @Override
  public RxSingleSchedulers get() {
    return Preconditions.checkNotNull(
        module.providesScheduler(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static RxModule_ProvidesSchedulerFactory create(RxModule module) {
    return new RxModule_ProvidesSchedulerFactory(module);
  }

  public static RxSingleSchedulers proxyProvidesScheduler(RxModule instance) {
    return Preconditions.checkNotNull(
        instance.providesScheduler(), "Cannot return null from a non-@Nullable @Provides method");
  }
}
