package com.telstra.android;

import android.databinding.DataBinderMapper;
import android.databinding.DataBindingComponent;
import android.databinding.ViewDataBinding;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import com.telstra.android.databinding.ActivityFeedListBindingImpl;
import com.telstra.android.databinding.EmptyItemViewModelBindingImpl;
import com.telstra.android.databinding.FeedItemsBindingImpl;
import com.telstra.android.databinding.SplashActivityBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_FEEDLIST = 1;

  private static final int LAYOUT_ITEMEMPTYVIEW = 2;

  private static final int LAYOUT_ITEMFEEDVIEW = 3;

  private static final int LAYOUT_SPLASHSCREEN = 4;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(4);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.telstra.android.R.layout.feed_list, LAYOUT_FEEDLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.telstra.android.R.layout.item_empty_view, LAYOUT_ITEMEMPTYVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.telstra.android.R.layout.item_feed_view, LAYOUT_ITEMFEEDVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.telstra.android.R.layout.splash_screen, LAYOUT_SPLASHSCREEN);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_FEEDLIST: {
          if ("layout/feed_list_0".equals(tag)) {
            return new ActivityFeedListBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for feed_list is invalid. Received: " + tag);
        }
        case  LAYOUT_ITEMEMPTYVIEW: {
          if ("layout/item_empty_view_0".equals(tag)) {
            return new EmptyItemViewModelBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for item_empty_view is invalid. Received: " + tag);
        }
        case  LAYOUT_ITEMFEEDVIEW: {
          if ("layout/item_feed_view_0".equals(tag)) {
            return new FeedItemsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for item_feed_view is invalid. Received: " + tag);
        }
        case  LAYOUT_SPLASHSCREEN: {
          if ("layout/splash_screen_0".equals(tag)) {
            return new SplashActivityBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for splash_screen is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new com.android.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(6);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "feedsViewModel");
      sKeys.put(2, "feedItemViewModel");
      sKeys.put(3, "splashview");
      sKeys.put(4, "emptyFeedView");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(4);

    static {
      sKeys.put("layout/feed_list_0", com.telstra.android.R.layout.feed_list);
      sKeys.put("layout/item_empty_view_0", com.telstra.android.R.layout.item_empty_view);
      sKeys.put("layout/item_feed_view_0", com.telstra.android.R.layout.item_feed_view);
      sKeys.put("layout/splash_screen_0", com.telstra.android.R.layout.splash_screen);
    }
  }
}
