// Generated by Dagger (https://google.github.io/dagger).
package com.telstra.android.di.module;

import com.telstra.android.api.ApiEndPoint;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;
import retrofit2.Retrofit;

public final class ApiModule_ProvideNewsApiFactory implements Factory<ApiEndPoint> {
  private final ApiModule module;

  private final Provider<Retrofit> retrofitProvider;

  public ApiModule_ProvideNewsApiFactory(ApiModule module, Provider<Retrofit> retrofitProvider) {
    this.module = module;
    this.retrofitProvider = retrofitProvider;
  }

  @Override
  public ApiEndPoint get() {
    return Preconditions.checkNotNull(
        module.provideNewsApi(retrofitProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static ApiModule_ProvideNewsApiFactory create(
      ApiModule module, Provider<Retrofit> retrofitProvider) {
    return new ApiModule_ProvideNewsApiFactory(module, retrofitProvider);
  }

  public static ApiEndPoint proxyProvideNewsApi(ApiModule instance, Retrofit retrofit) {
    return Preconditions.checkNotNull(
        instance.provideNewsApi(retrofit),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}
