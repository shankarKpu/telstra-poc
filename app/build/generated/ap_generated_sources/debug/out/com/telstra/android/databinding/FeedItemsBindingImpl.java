package com.telstra.android.databinding;
import com.telstra.android.R;
import com.telstra.android.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FeedItemsBindingImpl extends FeedItemsBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.arrow_img, 4);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FeedItemsBindingImpl(@Nullable android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private FeedItemsBindingImpl(android.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (android.widget.ImageView) bindings[4]
            , (android.support.v7.widget.CardView) bindings[0]
            , (android.widget.TextView) bindings[3]
            , (android.widget.ImageView) bindings[2]
            , (android.widget.TextView) bindings[1]
            );
        this.cardView.setTag(null);
        this.descriptionTxt.setTag(null);
        this.feedImg.setTag(null);
        this.titleTxt.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.feedItemViewModel == variableId) {
            setFeedItemViewModel((com.telstra.android.ui.landing.viewmodel.ItemViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setFeedItemViewModel(@Nullable com.telstra.android.ui.landing.viewmodel.ItemViewModel FeedItemViewModel) {
        this.mFeedItemViewModel = FeedItemViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.feedItemViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeFeedItemViewModelMTitle((android.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeFeedItemViewModelMDescription((android.databinding.ObservableField<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeFeedItemViewModelMImageUrl((android.databinding.ObservableField<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeFeedItemViewModelMTitle(android.databinding.ObservableField<java.lang.String> FeedItemViewModelMTitle, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeFeedItemViewModelMDescription(android.databinding.ObservableField<java.lang.String> FeedItemViewModelMDescription, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeFeedItemViewModelMImageUrl(android.databinding.ObservableField<java.lang.String> FeedItemViewModelMImageUrl, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.databinding.ObservableField<java.lang.String> feedItemViewModelMTitle = null;
        android.databinding.ObservableField<java.lang.String> feedItemViewModelMDescription = null;
        java.lang.String feedItemViewModelMTitleGet = null;
        java.lang.String feedItemViewModelMImageUrlGet = null;
        com.telstra.android.ui.landing.viewmodel.ItemViewModel feedItemViewModel = mFeedItemViewModel;
        android.databinding.ObservableField<java.lang.String> feedItemViewModelMImageUrl = null;
        java.lang.String feedItemViewModelMDescriptionGet = null;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (feedItemViewModel != null) {
                        // read feedItemViewModel.mTitle
                        feedItemViewModelMTitle = feedItemViewModel.mTitle;
                    }
                    updateRegistration(0, feedItemViewModelMTitle);


                    if (feedItemViewModelMTitle != null) {
                        // read feedItemViewModel.mTitle.get()
                        feedItemViewModelMTitleGet = feedItemViewModelMTitle.get();
                    }
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (feedItemViewModel != null) {
                        // read feedItemViewModel.mDescription
                        feedItemViewModelMDescription = feedItemViewModel.mDescription;
                    }
                    updateRegistration(1, feedItemViewModelMDescription);


                    if (feedItemViewModelMDescription != null) {
                        // read feedItemViewModel.mDescription.get()
                        feedItemViewModelMDescriptionGet = feedItemViewModelMDescription.get();
                    }
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (feedItemViewModel != null) {
                        // read feedItemViewModel.mImageUrl
                        feedItemViewModelMImageUrl = feedItemViewModel.mImageUrl;
                    }
                    updateRegistration(2, feedItemViewModelMImageUrl);


                    if (feedItemViewModelMImageUrl != null) {
                        // read feedItemViewModel.mImageUrl.get()
                        feedItemViewModelMImageUrlGet = feedItemViewModelMImageUrl.get();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.descriptionTxt, feedItemViewModelMDescriptionGet);
        }
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            com.telstra.android.api.model.Row.loadImage(this.feedImg, feedItemViewModelMImageUrlGet);
        }
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.titleTxt, feedItemViewModelMTitleGet);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): feedItemViewModel.mTitle
        flag 1 (0x2L): feedItemViewModel.mDescription
        flag 2 (0x3L): feedItemViewModel.mImageUrl
        flag 3 (0x4L): feedItemViewModel
        flag 4 (0x5L): null
    flag mapping end*/
    //end
}