package com.telstra.android.databinding;
import com.telstra.android.R;
import com.telstra.android.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class EmptyItemViewModelBindingImpl extends EmptyItemViewModelBinding implements com.telstra.android.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.tv_message, 2);
    }
    // views
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public EmptyItemViewModelBindingImpl(@Nullable android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 3, sIncludes, sViewsWithIds));
    }
    private EmptyItemViewModelBindingImpl(android.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.Button) bindings[1]
            , (android.widget.LinearLayout) bindings[0]
            , (android.widget.TextView) bindings[2]
            );
        this.btnRetry.setTag(null);
        this.linearLayoutView.setTag(null);
        setRootTag(root);
        // listeners
        mCallback1 = new com.telstra.android.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.emptyFeedView == variableId) {
            setEmptyFeedView((com.telstra.android.ui.landing.viewmodel.EmptyItemViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setEmptyFeedView(@Nullable com.telstra.android.ui.landing.viewmodel.EmptyItemViewModel EmptyFeedView) {
        this.mEmptyFeedView = EmptyFeedView;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.emptyFeedView);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.telstra.android.ui.landing.viewmodel.EmptyItemViewModel emptyFeedView = mEmptyFeedView;
        // batch finished
        if ((dirtyFlags & 0x2L) != 0) {
            // api target 1

            this.btnRetry.setOnClickListener(mCallback1);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // emptyFeedView
        com.telstra.android.ui.landing.viewmodel.EmptyItemViewModel emptyFeedView = mEmptyFeedView;
        // emptyFeedView != null
        boolean emptyFeedViewJavaLangObjectNull = false;



        emptyFeedViewJavaLangObjectNull = (emptyFeedView) != (null);
        if (emptyFeedViewJavaLangObjectNull) {


            emptyFeedView.onRetryClick();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): emptyFeedView
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}