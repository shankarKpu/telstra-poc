package com.telstra.android.databinding;
import com.telstra.android.R;
import com.telstra.android.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityFeedListBindingImpl extends ActivityFeedListBinding implements com.telstra.android.generated.callback.OnRefreshListener.Listener {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.activityMain, 3);
        sViewsWithIds.put(R.id.toolbar, 4);
        sViewsWithIds.put(R.id.progress, 5);
    }
    // views
    // variables
    @Nullable
    private final android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener mCallback2;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityFeedListBindingImpl(@Nullable android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private ActivityFeedListBindingImpl(android.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (android.widget.LinearLayout) bindings[3]
            , (android.support.design.widget.CoordinatorLayout) bindings[0]
            , (android.support.v7.widget.RecyclerView) bindings[2]
            , (android.widget.ProgressBar) bindings[5]
            , (android.support.v4.widget.SwipeRefreshLayout) bindings[1]
            , (android.support.v7.widget.Toolbar) bindings[4]
            );
        this.clRootView.setTag(null);
        this.feedsRecyclerView.setTag(null);
        this.swipeToRefresh.setTag(null);
        setRootTag(root);
        // listeners
        mCallback2 = new com.telstra.android.generated.callback.OnRefreshListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.feedsViewModel == variableId) {
            setFeedsViewModel((com.telstra.android.ui.landing.viewmodel.FeedListViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setFeedsViewModel(@Nullable com.telstra.android.ui.landing.viewmodel.FeedListViewModel FeedsViewModel) {
        this.mFeedsViewModel = FeedsViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.feedsViewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeFeedsViewModelMFeedList((android.databinding.ObservableList<com.telstra.android.api.model.Row>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeFeedsViewModelMFeedList(android.databinding.ObservableList<com.telstra.android.api.model.Row> FeedsViewModelMFeedList, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.telstra.android.ui.landing.viewmodel.FeedListViewModel feedsViewModel = mFeedsViewModel;
        android.databinding.ObservableList<com.telstra.android.api.model.Row> feedsViewModelMFeedList = null;

        if ((dirtyFlags & 0x7L) != 0) {



                if (feedsViewModel != null) {
                    // read feedsViewModel.mFeedList
                    feedsViewModelMFeedList = feedsViewModel.mFeedList;
                }
                updateRegistration(0, feedsViewModelMFeedList);
        }
        // batch finished
        if ((dirtyFlags & 0x7L) != 0) {
            // api target 1

            com.telstra.android.utils.BindingUtils.addRowItem(this.feedsRecyclerView, feedsViewModelMFeedList);
        }
        if ((dirtyFlags & 0x4L) != 0) {
            // api target 1

            this.swipeToRefresh.setOnRefreshListener(mCallback2);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnRefresh(int sourceId ) {
        // localize variables for thread safety
        // feedsViewModel
        com.telstra.android.ui.landing.viewmodel.FeedListViewModel feedsViewModel = mFeedsViewModel;
        // feedsViewModel != null
        boolean feedsViewModelJavaLangObjectNull = false;



        feedsViewModelJavaLangObjectNull = (feedsViewModel) != (null);
        if (feedsViewModelJavaLangObjectNull) {


            feedsViewModel.onRefresh();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): feedsViewModel.mFeedList
        flag 1 (0x2L): feedsViewModel
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}